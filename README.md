# Ubuntu Touch for SHIFT6mq (axolotl)

## Build images

```
export DEVICE="$(source deviceinfo && echo $deviceinfo_codename)"
export XZ_DEFAULTS="-T 0"
./build.sh -b . -o out
./build/prepare-fake-ota.sh out/device_$DEVICE.tar.xz ota
./build/system-image-from-ota.sh ota/ubuntu_command ota_out
```

## Requirements

The device needs to be bootstrapped in order to work with Ubuntu Touch.

For that you need to unlock the bootloader and repartition the super partition.

You can find prebuilt images at the [assets repository](https://gitlab.com/SHIFTPHONES/ubports/assets).

## Flash images

First you need to install the UBports recovery and reboot into fastbootd<br>
1) Power off device
2) Hold POWER + VOLUME UP to boot into bootloader mode
3) `fastboot flash recovery recovery.img`
5) `fastboot reboot fastboot`

Once you are in fastbootd mode, wipe super and repartition it:
- `fastboot wipe-super super_mainline.img`
- `fastboot reboot fastboot`

Next flash the following prebuilt images in fastbootd mode
```
fastboot flash vendor vendor.img
```

Then flash the images you built yourself using this repo:
```
fastboot flash boot   ota_out/boot.img
fastboot flash dtbo   ota_out/dtbo.img
fastboot flash system ota_out/system.img
```

You also need to wipe userdata. This can be done directly in `fastbootd` mode or via recovery.

### Wipe data in recovery

While in `fastbootd` mode, select `Enter recovery`.<br>
This will reboot into recovery, where you have to select `Wipe data/factory reset`.<br>
Confirm the selection with selecting `Factory data reset`.

## (Development) Test installation via Recovery

Follow the above section but only install the recovery image, push the resulting ota files
to the device and reboot recovery to start installation.

1) `fastboot flash recovery recovery.img`
2) `fastboot reboot recovery`
3) Once recovery is booted, enter `Advanced` and `Enable ADB`
4) `adb push ota/ /cache/recovery`
5) `adb reboot recovery`

## Restore stock

You can obtain `super_empty.img` from the assets repository linked above.
A full-ota is also required - it can be obtained from the [SHIFTPHONES downloads portal](https://downloads.shiftphones.com/public/).

Extract the stock recovery from the full-ota payload with tools such as:
  - https://github.com/vm03/payload_dumper
  - https://github.com/cyxx/extract_android_ota_payload
  - Nag @amartinz until he finally adjusts the stock release scripts to also release the images separatly together with each OTA

1) Power off device
2) Hold POWER + VOLUME UP to boot into bootloader mode
3) `fastboot flash recovery recovery_stock.img`
4) `fastboot reboot fastboot`
5) `fastboot wipe-super super_empty.img`
